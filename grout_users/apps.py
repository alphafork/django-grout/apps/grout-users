from django.apps import AppConfig


class GroutUsersConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "grout_users"
